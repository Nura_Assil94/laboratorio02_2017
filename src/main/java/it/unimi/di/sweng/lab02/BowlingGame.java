package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	private static final int MAX_ROLLS = 20;
	private int[] rolls = new int[MAX_ROLLS];
	private int currentRoll = 0;
	
	@Override
	public void roll(int pins) {
		rolls[currentRoll++] = pins;
		if(pins == 10 && currentRoll%2 == 1)
			rolls[currentRoll++] = 0;
	}

	@Override
	public int score() {
		int score = 0;
		
		for(int currentRoll=0; currentRoll<MAX_ROLLS; currentRoll++){
			if(currentRoll<18 && rolls[currentRoll] == 10 && currentRoll%2==0){
				score += rolls[currentRoll+2] + rolls[currentRoll+3];
			}
			else if(currentRoll<19 && currentRoll%2==0 && rolls[currentRoll] + rolls[currentRoll+1] == 10)
				score += rolls[currentRoll+2];
			score += rolls[currentRoll];
		}
		
		return score;
	}

}
